#include <iostream>

#include "bitmap_image.hpp"
#include <math.h>

int main(int argc, char** argv) {

    //initialise point variables. if we needed more points or a dynamic amount, it would be better to use an array or a list here.
    float x1, x2, x3, y1, y2, y3, r1, r2, r3, g1, g2, g3, b1, b2, b3;

    //prompt the user for inputs, and then collect the three points and their colors.
    std::cout << "Please enter three points and their colors, each formatted as: x y r g b.";
    std::cin >> x1 >> y1 >> r1 >> g1 >> b1;
    std::cin >> x2 >> y2 >> r2 >> g2 >> b2;
    std::cin >> x3 >> y3 >> r3 >> g3 >> b3;

    // create an image 640 pixels wide by 480 pixels tall
    bitmap_image image(640, 480);

    //color the image black
    image.set_all_channels(0,0,0);

    //draw the image, then place small circles at each point. this is used for reference and saved as a separate image from triangle
    image_drawer draw(image);
    draw.pen_color(255,255,255);
    draw.circle(x1,y1,3);
    draw.circle(x2,y2,3);
    draw.circle(x3,y3,3);
    image.save_image("points.bmp");
    image.set_all_channels(0,0,0
    );

    //begin drawing the triangle pixel by pixel
    for (int i = 0; i< 640; i++)
    {
        for (int j = 0; j < 480; j++)
        {
            //at each pixel, assign barymetric coordinate as per the functions below.
            float beta = ((y2 - y3)*(i - x3) + (x3 - x2)*(j - y3))/((y2 - y3)*(x1 - x3) + (x3 - x2) *(y1 - y3));
            float gamma = ((y3 - y1)* (i - x3) + (x1 - x3) *(j - y3))/((y2 - y3)* (x1 - x3) + (x3 - x2) *(y1 - y3));
            float alpha = 1 - beta - gamma;

            //if all of the coordinates are between 1 and 0 (inside the triangle), we color them according to the user's given colors, and their location in the triangle.
            if (0 <= alpha && alpha <= 1 && 0 <= beta && beta <= 1 && 0 <= gamma && gamma <= 1) {

                //calculate the color of a pixel by multiplying the specific component by it's barymetric weight from each point and
                //multiplying the final interpolation (between 0 and 1) by 255 to determine the intensity
                float r = abs(r1*alpha + r2*beta + r3*gamma) * 255;
                float g = abs(g1*alpha + g2* beta + g3* gamma) * 255;
                float b = abs(b1*alpha + b2*beta + b3*gamma) * 255;

                image.set_pixel(i,j,r,g,b);
                image_drawer draw(image);
            }
        }
    }

    image.save_image("triangle.bmp");
    std::cout << "Success" << std::endl;
}
